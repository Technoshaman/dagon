// DaGoN by Technoshaman - 07/02/2013

// That is not dead which can eternal lie. And with strange aeons even death may die.
// H.P. Lovecraft

/* Notes:

 * GE (Google Earth) KML uses long-lat, instead of lat-long order
 * Move on to MathGL, drop gnuplot
 * Add pseudo terminal code to send GPS NMEA string to it for OpenCPN functionality

*/

#define _GNU_SOURCE
#include <gnuplot_i.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <termios.h> // serial port stuff
//#include <sys/ioctl.h> // serial port hack stuff
#include <sqlite3.h> // simple DB
#include <nmea/nmea.h> // NMEA parser, etc. library

#define MAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define BUF_SIZE 1024
#define BAUDRATE_RFM B38400
#define BAUDRATE_PSTM B9600
#define SER_DEVICE_RFM "/dev/ttyUSB0"
//#define SER_DEVICE_RFM "/dev/pts/4" // for gpsfake random point, check with ls /dev/pts

int main(void)
{
	const char *sql_insert = "insert into RF_Data (Date, Time,  TEC_Dpt, TEC_Hdg, TEC_Spd, TEC_Lon, TEC_Lat, TEC_Dis, TEC_Bat, TEC_Eat) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	const char *sql_read = "select TEC_Lon, TEC_Lat, TEC_Dpt from RF_Data order by Id desc limit 3600"; // only get the last 3600 path coordinates = 1 hour
	const char *sql_read2 = "select TEC_Lon, TEC_Lat, TEC_Dpt from RF_Data order by Id"; // get all for plot
	const char *sql_crt_tab = "create table if not exists RF_data (Id INTEGER PRIMARY KEY, Date TEXT, Time TEXT, TEC_Dpt NUMERIC, TEC_Hdg NUMERIC, TEC_Spd NUMERIC, TEC_Lon NUMERIC, TEC_Lat NUMERIC, TEC_Dis NUMERIC, TEC_Bat NUMERIC, TEC_Eat NUMERIC)"; // create table if DB file not present

	struct _Tech_Ne {
		double Lat;		// Boat Latitude
		double Lon;		// Boat Longitude
		double Spd;		// Boat Speed
		double Hdg;		// Boat Heading
		double Dpt;		// Sonar Depth
		double Dis;		// Path Length
		int Bat;		// Boat Battery
		int Eat;	    	// Electronics Battery
		char IrA[7];	    	// IR Sensor A
		char IrB[7];	    	// IR Sensor B
	} TEC;

	double STA_Lat; // Base station lat,
	double STA_Lon; // lon

	static char buf[BUF_SIZE];
	char buff_RFM[128];
	char *buff_GPS_prev,*buff_RFMt, *RF_message = "No message", *KB_message = "No input";
	char cDate[11], cTime[9], keyPut[8];
	char *buff_GPSt, *buff_GPS, *buff_BAT, *buff_EAT, *buff_DPT, *buff_IRA, *buff_IRB, *buff_TEM; // Values from parsed RF string, buff_RFM[128]
	int ret, fd_RFM, num, ret_val, pstm, rc;
	unsigned int  maxfd, lineColor, secs = 0, total_secs = 0, RSSI = 75;
	double path_len;
	bool first_round = true, loopC = true;
	struct termios newtio_RFM, oldtio;
	struct termios newtio_pstm, oldtio_pstm;
	fd_set readfs;
	struct timeval tv;
	FILE *pKML, *pPLOT;

	sqlite3_stmt *stmt;
	sqlite3 *conn;

	nmeaINFO ninfo, info_prev;
	nmeaPARSER nparser, parser_prev;
	nmeaPOS dpos, pos_prev;
	nmea_zero_INFO(&ninfo);
	nmea_zero_INFO(&info_prev);

	nmea_parser_init(&nparser);
	nmea_parser_init(&parser_prev);

	TEC.Dis = 0.00;

	pstm = posix_openpt(O_RDWR); // Open a pseudo terminal
	if (pstm < 0)
	{
		perror("posix_openpt()");
		exit(-1);
	}

	rc = grantpt(pstm);
	if (rc != 0)
	{
		perror("grantpt()");
		exit(-1);
	}

	rc = unlockpt(pstm);
	if (rc != 0)
	{
		perror("unlockpt()");
		exit(-1);
	}

	printf("\n\t\t\t\t%c[%d;%dmCTHULHU FHTAGN!%c[%dm\n", 27, 1, 31, 27 ,0);
	printf("\t\t\t\t%c[%d;%dmPseudo Terminal:%c[%dm %s\n\n", 27, 1, 31, 27 ,0, ptsname(pstm));
	printf("\t\t\t\t%c[%d;%dmPress any key to start...%c[%dm\n\n", 27, 1, 32, 27 ,0);
	getchar();

	tcgetattr(pstm, &oldtio_pstm); // Save existing serial port settings for pts
        bzero(&newtio_pstm, sizeof(newtio_pstm));

        newtio_pstm.c_cflag |= (BAUDRATE_PSTM | CLOCAL | CREAD | CS8); // Make pts raw
        newtio_pstm.c_iflag |= (IGNBRK | IGNPAR | IGNCR);
        newtio_pstm.c_lflag |= (ICANON);

        tcflush(pstm, TCIFLUSH);
        tcsetattr(pstm, TCSANOW, &newtio_pstm);


	fd_RFM = open(SER_DEVICE_RFM, O_RDWR | O_NOCTTY); // Initialize serial port /dev/ttyUSBx for line receive mode
	if(fd_RFM < 0)
	{
		perror(SER_DEVICE_RFM);
	        exit(-1);
	}

	tcgetattr(fd_RFM, &oldtio); // Save existing serial port settings
	bzero(&newtio_RFM, sizeof(newtio_RFM));

	newtio_RFM.c_cflag |= (BAUDRATE_RFM | CLOCAL | CREAD | CS8);
	newtio_RFM.c_iflag |= (IGNBRK | IGNPAR | IGNCR);
	newtio_RFM.c_lflag |= (ICANON);

	tcflush(fd_RFM, TCIFLUSH);
	tcsetattr(fd_RFM, TCSANOW, &newtio_RFM);

	ret = sqlite3_open("data.sl3", &conn); // Open DB
	if(ret)
	{
		fprintf(stderr, "Can not open database: %s\n", sqlite3_errmsg(conn));
		exit(0);
	}

	ret = sqlite3_prepare_v2(conn, sql_crt_tab, -1, &stmt, NULL); // Prepare insert statement here
	if(ret != SQLITE_OK)
	{
		fprintf(stderr, "Create Table Statement Prepare: %s\n", sqlite3_errmsg(conn));
		exit(0);
	}

	ret = sqlite3_step(stmt); // Execute insertion
	if(ret != SQLITE_DONE)
        {
                fprintf(stderr, "Create Table Action: %s\n", sqlite3_errmsg(conn));
       	        exit(0);
        }
	sqlite3_reset(stmt);

	/***************************************************** MAIN LOOP STARTS *****************************************************/

	gnuplot_ctrl *handle;
	handle = gnuplot_init();

do
{
	time_t t = time(NULL);
	struct tm myClock = *localtime(&t);

	maxfd = MAX(fd_RFM, STDIN_FILENO) + 1;

        FD_ZERO(&readfs);
        FD_SET(fd_RFM, &readfs);
        FD_SET(STDIN_FILENO, &readfs);

	tv.tv_sec = 1;
        tv.tv_usec = 0; // 1s timeout for select()

	ret_val = select(maxfd, &readfs, NULL, NULL, &tv);

	if(ret_val)
	{
        	if(FD_ISSET(fd_RFM, &readfs))
		{
			num = read(fd_RFM, &buff_RFM, sizeof(buff_RFM));

			if(num > 50 && num < 100)
			{
				buff_RFM[num-1] = 0;  // Remove the \n
				buff_RFMt = strdup(buff_RFM);

				buff_GPSt = strtok(buff_RFMt, "+"); // Now buff_GPSt points to buff_RFMt's address
			        buff_DPT = strtok(NULL, "+");
			        buff_BAT = strtok(NULL, "+");
			        buff_EAT = strtok(NULL, "+");
			        buff_IRA = strtok(NULL, "+");
			        buff_IRB = strtok(NULL, "+");
			        buff_TEM = strtok(NULL, "+");

				buff_GPS = malloc(strlen(buff_GPSt) + 3); // 3 for \r\n\0
			        if(buff_GPS == NULL)
			       	{
			                perror(buff_GPS);
			                exit(-1);
			       	}
	        		strcpy(buff_GPS, buff_GPSt);
			        strcat(buff_GPS,"\r\n"); // Make NMEA sentence suitable for NMEA lib

				write(pstm, buff_GPS, strlen(buff_GPS)); // Send GPS NMEA to pseudo terminal for OpenCPN use

				printf("\033[2J\033[1;1H"); // Clear screen

				printf("\n\t%c[%d;%dm+------------------------------------------------------<%06lld>------------------------------------------------------+%c[%dm\n\n", 27, 1, 36, sqlite3_last_insert_rowid(conn) + 1, 27, 0);
				printf("\t%c[%d;%dm  CHARS READ:%c[%dm %d  %c[%d;%dmNMEA LENGTH:%c[%dm %d  %c[%d;%dmNMEA SENTENCE:%c[%dm %s", 27, 1, 31, 27, 0, num, 27, 1, 31, 27, 0, (int)strlen(buff_GPS), 27, 1, 31, 27, 0, buff_GPS);
				printf("\n\t%c[%d;%dm+--------------------------------------------------------------------------------------------------------------------+%c[%dm\n\n", 27, 1, 36, 27, 0);

				nmea_parse(&nparser, buff_GPS, strlen(buff_GPS), &ninfo); // Parse NMEA sentence - All praise NMEA Library!

				if(first_round) // If this is the first loop, we got no previous position info, so use the new one twice for zero distance
				{
					buff_GPS_prev = strdup(buff_GPS);
					first_round = false;
					STA_Lat = nmea_ndeg2degree(ninfo.lat); // Also set the starting point for the boat
					STA_Lon = nmea_ndeg2degree(ninfo.lon);
				}

				nmea_parse(&parser_prev, buff_GPS_prev, strlen(buff_GPS_prev), &info_prev); // Parse previous NMEA sentence
				buff_GPS_prev = strdup(buff_GPS);

				free(buff_GPS);
				buff_GPS = NULL;

				nmea_info2pos(&ninfo, &dpos); // Get pos struct for distance calculations
				nmea_info2pos(&info_prev, &pos_prev);
				path_len = nmea_distance(&pos_prev, &dpos); // Calculate distance
				TEC.Dis += path_len; // Add to total path

				TEC.Lat = nmea_ndeg2degree(ninfo.lat); // Assign the  results from parser to variables
				TEC.Lon = nmea_ndeg2degree(ninfo.lon);
			        TEC.Spd = ninfo.speed;
			        TEC.Hdg = ninfo.direction;

				snprintf(TEC.IrA, 7, "%s", (atoi(buff_IRA) < 500) ? "Alarm!" : "Normal"); // Check for IR sensors
				snprintf(TEC.IrB, 7, "%s", (atoi(buff_IRB) < 500) ? "Alarm!" : "Normal");

				// Vbat = Vcc x (ADC reading)/(2^ADC resolution)

				TEC.Bat = atoi(buff_BAT) * 125 / 4096; // 80% is full, since ADC set to read up to 10.5 V and the Bat V Max is 8.4V
				TEC.Eat = atoi(buff_EAT) * 125 / 4096;
				TEC.Dpt = atof(buff_DPT) * 0.3048; // ft to m conversion for more resolution

				lineColor = (TEC.Dpt * -0.423 + 127); // Map depth to color range -0.423 = -127 / 300
				// Terminal encoding should be set to ISO-8859-X for degree sign to appear
				printf("\t  %c[%d;%dm[GPS DATA]%c[%dm\tLat: %f\tLon: %f \tHdg: %1.2f%c \tSpd: %1.1f km/h \tSig: %d \tFix: %d\n\n",
					27, 1, 36, 27, 0, TEC.Lat, TEC.Lon, TEC.Hdg, 176, TEC.Spd, ninfo.sig, ninfo.fix);
				printf("\t  %c[%d;%dm[PHYSICAL]%c[%dm\tDepth: %1.2f m \tDist: %1.2f m \tWater Temp: %s%cC \n\n\t  %c[%d;%dm[XTRONICS]%c[%dm\tBat: %%%d \tEat: %%%d \tIrA: %s \tIrB: %s \tRF Sig Strength: %%%d\n",
					27, 1, 36, 27, 0, TEC.Dpt, TEC.Dis, buff_TEM, 176, 27, 1, 36, 27, 0, TEC.Bat, TEC.Eat, TEC.IrA, TEC.IrB, RSSI);
				printf("\n\t%c[%d;%dm+--------------------------------------------------------------------------------------------------------------------+%c[%dm\n\n", 27, 1, 36, 27, 0);
				printf("\t%c[%d;%dm  CONSOLE INPUT:%c[%dm %s", 27, 1, 31, 27, 0, KB_message);
				printf("\t%c[%d;%dmTOTAL CONNECTION LOSS:%c[%dm %d seconds", 27, 1, 31, 27, 0, total_secs);
				printf("\t%c[%d;%dm  PSEUDO TERMINAL:%c[%dm %s\n\n", 27, 1, 31, 27, 0, ptsname(pstm));
				printf("\t%c[%d;%dm  LAST RX RF MESSAGE:%c[%dm %s\n\n", 27, 1, 31, 27, 0, RF_message);
				printf("\t%c[%d;%dm+-----------------------------------------------[%d/%02d/%02d][%02d:%02d:%02d]-----------------------------------------------+%c[%dm\n\n", 27, 1, 36, myClock.tm_mday, myClock.tm_mon + 1, myClock.tm_year + 1900, myClock.tm_hour, myClock.tm_min, myClock.tm_sec, 27, 0);

				ret = sqlite3_prepare_v2(conn, sql_insert, -1, &stmt, NULL); // Prepare insert statement here
				if(ret != SQLITE_OK)
				{
					fprintf(stderr, "Insert Statement Prepare: %s\n", sqlite3_errmsg(conn));
					exit(0);
				}

				snprintf(cDate, 11, "%02d/%02d/%d", myClock.tm_mday, myClock.tm_mon + 1, myClock.tm_year + 1900);
				snprintf(cTime, 9, "%02d:%02d:%02d", myClock.tm_hour, myClock.tm_min, myClock.tm_sec);

				sqlite3_bind_text(stmt, 1, cDate, strlen(cDate), SQLITE_STATIC); // Bind values to parameters
				sqlite3_bind_text(stmt, 2, cTime, strlen(cTime), SQLITE_STATIC);
				sqlite3_bind_double(stmt, 3, TEC.Dpt);
				sqlite3_bind_double(stmt, 4, TEC.Hdg);
				sqlite3_bind_double(stmt, 5, TEC.Spd);
				sqlite3_bind_double(stmt, 6, TEC.Lon);
				sqlite3_bind_double(stmt, 7, TEC.Lat);
				sqlite3_bind_double(stmt, 8, TEC.Dis);
				sqlite3_bind_int(stmt, 9, TEC.Bat);
				sqlite3_bind_int(stmt, 10, TEC.Eat);

				ret = sqlite3_step(stmt); // Execute insertion
				if(ret != SQLITE_DONE)
			        {
			                fprintf(stderr, "Insert Action: %s\n", sqlite3_errmsg(conn));
		        	        exit(0);
			        }
				sqlite3_reset(stmt);

				ret = sqlite3_prepare_v2(conn, sql_read, -1, &stmt, NULL); // Prepare to read data from table
				if(ret != SQLITE_OK)
				{
					fprintf(stderr, "Read Statement Prepare: %s\n", sqlite3_errmsg(conn));
					exit(0);
				}

			   	pKML = fopen("RF_Data.kml", "w"); // Open KLM file for write only
			   	if(setvbuf(pKML, buf, _IOLBF, BUF_SIZE) != 0)
					perror("setvbuf()");

				fprintf(pKML, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" // Start writing KML
				"<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n"
				"<Document>\n"
				"\t<name>Dagon's Path</name>\n"
				"\t<open>1</open>\n"
				"\t<Snippet maxLines=\"2\"><![CDATA[Visit <A href=\"http://www.technoshamanarchist.net\">Technoshaman</A>]]></Snippet>\n"
				"\t<Folder id=\"Tracks\">\n"
				"\t\t<name>%s - %s</name>\n"
				"\t\t<open>1</open>\n"
				"\t\t<Placemark>\n"
				"\t\t\t<name>İzlenen Yol</name>\n"
				"\t\t\t<Snippet maxLines=\"0\"></Snippet>\n"
				"\t\t\t<description>Gidiyoruz...</description>\n"
				"\t\t\t<Style>\n"
				"\t\t\t\t<LineStyle>\n"
				"\t\t\t\t\t<color>ff%02x%02xff</color>\n"	// Alpha + Blue + Green + Red (get string value from depth, convert to double, Alpha and ,B are static,
				"\t\t\t\t\t<width>2</width>\n"			// increase G + R together to lighten B, map double depth to int colors, convert to hex, and insert here)
				"\t\t\t\t</LineStyle>\n"
				"\t\t\t</Style>\n"
				"\t\t\t<LineString>\n"
				"\t\t\t\t<tessellate>1</tessellate>\n"
				"\t\t\t\t<coordinates>\n", cDate, cTime, lineColor, lineColor); // Written permanent first part of the file

				while (sqlite3_step(stmt) == SQLITE_ROW) // Append the path line coordinates and depth from DB
				{
					fprintf(pKML, "\t\t\t\t%f,%f,%1.2f\n", sqlite3_column_double(stmt, 0), sqlite3_column_double(stmt, 1), 500 - sqlite3_column_double(stmt, 2));
				} // No space after the comma between the coordinates, or GE gets nothing!
				sqlite3_reset(stmt);

				fprintf(pKML, "\t\t\t\t</coordinates>\n" // Append the rest of the data in kml

				"\t\t\t\t<altitudeMode>clampToGround</altitudeMode>\n" // "clampToGround" to ignore, "absolute"to utilize
				"\t\t\t</LineString>\n"
				"\t\t</Placemark>\n"

				/* Stable icon, where the control station is located  - This will be set at the starting point */
				"\t\t<Placemark>\n"
				"\t\t\t<name>İstasyon</name>\n"
				"\t\t\t<description>Konum: %f, %f</description>\n" // Base coordinates first
				"\t\t\t<Style>\n"
				"\t\t\t\t<IconStyle>\n"
				"\t\t\t\t\t<Icon>\n"
				"\t\t\t\t\t\t<href>dock.png</href>\n"
				"\t\t\t\t\t</Icon>\n"
				"\t\t\t\t</IconStyle>\n"
				"\t\t\t</Style>\n"
				"\t\t\t<Point>\n"
				"\t\t\t\t<coordinates>%f, %f</coordinates>\n" // Base coordinates again
				"\t\t\t</Point>\n"
				"\t\t</Placemark>\n"

				/* Active icon, navigating boat - This will be upadated as the boat moves, heading (from GPS, or Compass?) and coordinates */
				"\t\t<Placemark>\n"
				"\t\t\t<name>Tech-Ne</name>\n"
				"\t\t\t<Snippet maxLines=\"12\"><![CDATA[<b>Konum:</b> %f, %f<br><b>Hız:</b> %1.2f km/h<br><b>Yön:</b> %1.2f &deg;<br><b>"
				"Derinlik:</b> %1.2f m<br><b>Su Sıcaklığı:</b>  %s &deg;C<br><b>Toplam Mesafe:</b> %1.2f m<br><b>Tekne Pil:</b> %% %d<br><b>Elektro Pil:</b> %% %d<br><b>IR A:</b> %s<br><b>IR B:</b> %s<br><b>RF Sinyal Gücü:</b> %%%d< ]]></Snippet>\n"
				"\t\t\t<description>&amp;nbsp;</description>\n"
				// Detailed info here, coordinates, speed, heading, covered distance, battery state, IR sensor state, etc.^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				"\t\t\t<Style>\n"
				"\t\t\t\t<IconStyle>\n"
				"\t\t\t\t\t<heading>%f</heading>\n" // Heading also here for Boat Icon
		       		/* Update this heading for moving boat */
				"\t\t\t\t\t<scale>0.5</scale>\n"
				"\t\t\t\t\t<Icon>\n"
				"\t\t\t\t\t\t<href>boat.png</href>\n"
				"\t\t\t\t\t</Icon>\n"
				"\t\t\t\t</IconStyle>\n"
				"\t\t\t</Style>\n"
				"\t\t\t<Point>\n"
				"\t\t\t\t<coordinates>%f, %f</coordinates>\n" // Boat coordinates here
		       		/* Update this coordinate pair for a moving boat */
				"\t\t\t</Point>\n"
				"\t\t</Placemark>\n"
				"\t</Folder>\n"
				"</Document>\n"
				"</kml>\n"
				"<!-- \"KML code generated by Dagon x86 - Technoshaman - 2013\" -->", STA_Lat, STA_Lon, STA_Lon, STA_Lat, TEC.Lat,
				TEC.Lon, TEC.Spd, TEC.Hdg, TEC.Dpt, buff_TEM, TEC.Dis, TEC.Bat, TEC.Eat, TEC.IrA, TEC.IrB, RSSI, TEC.Hdg, TEC.Lon, TEC.Lat); // Reverse coordinate orders

				fclose(pKML);
				secs = 0;
			}
			else if(num < 45)
			{
				buff_RFM[num-1] = 0;  // Null terminate and kill newline
				RF_message = malloc(strlen(buff_RFM)+1);
				strcpy(RF_message, buff_RFM);
			}

			lseek(fd_RFM, 0, SEEK_SET);
		}
		if(FD_ISSET(STDIN_FILENO, &readfs))
		{
			fflush(stdin);
			fgets(keyPut, sizeof(keyPut), stdin); // "\n\0" added automatically

			switch(keyPut[0])
			{
				case 'Q':
				loopC = false;
				break;
				case 'P':
				printf("%c[%d;%dmPrinting plot file...\n%c[%dm", 27, 1, 31, 27, 0);
			   	pPLOT = fopen("plot.dat", "w"); // Open xyz data file for write only
				fprintf(pPLOT, "#X Y Z\n"); // Print header

				ret = sqlite3_prepare_v2(conn, sql_read2, -1, &stmt, NULL); // Prepare to read data from table
                                if(ret != SQLITE_OK)
                                {
                                        fprintf(stderr, "Read Statement Prepare: %s\n", sqlite3_errmsg(conn));
                                        exit(0);
                                }

				while (sqlite3_step(stmt) == SQLITE_ROW) // Append the depth coordinates and depth values from DB
				{
					fprintf(pPLOT, "%f %f -%1.2f\n", sqlite3_column_double(stmt, 0), sqlite3_column_double(stmt, 1), sqlite3_column_double(stmt, 2)); // Back to normal Lat Lon for gnuplot
				}
                                sqlite3_reset(stmt);

				fclose(pPLOT);
				printf("%c[%d;%dmPrinting done!\n%c[%dm", 27, 1, 31, 27, 0);
				break;
				default:
				write(fd_RFM, keyPut, strlen(keyPut));
				keyPut[strlen(keyPut)-1] = 0;  // Null terminate and kill newline
				KB_message = malloc(strlen(keyPut)+1);
				strcpy(KB_message, keyPut);
				break;
			}
		}
	}
	if(ret_val == 0)
	{
		printf("%c[%d;%dmNo RF data for %u seconds!%c[%dm\n",27, 1, 31, ++secs, 27, 0);
//		write(pstm, "Data error!\n", 13); // Send error message to pt
		++total_secs;
	}
	if(ret_val == -1)
		perror("select()");
}while(loopC);

	if(strlen(buff_GPS_prev) > 50)
	{
		free(buff_GPS_prev);
		buff_GPS_prev = NULL;
	}

	gnuplot_set_xlabel(handle, "Lon");
	gnuplot_set_ylabel(handle, "Lat");
	gnuplot_cmd(handle, "set zlabel \"Depth\"");
	gnuplot_cmd(handle, "set title \"Dagon Depth Map Plotted from Bathymetric Survey Data - Technoshaman 2013\"");
	gnuplot_cmd(handle, "set zrange [-300:0]");
	gnuplot_cmd(handle, "set size 0.56, 1.00"); // looks ok as proportion of lat/lon
	gnuplot_cmd(handle, "set border 4095");
	gnuplot_cmd(handle, "set key outside right top Left box 3");
	gnuplot_cmd(handle, "set key title \"Contour Depth Values\"");
	gnuplot_cmd(handle, "set colorbox user origin 0.8, 0.05");
//	gnuplot_cmd(handle, "set terminal png size 1920, 1080");
//	gnuplot_cmd(handle, "set output \"map.png\"");
//	gnuplot_cmd(handle, "unset key");
	gnuplot_cmd(handle, "set dgrid3d 200, 112, qnorm 4");
	gnuplot_cmd(handle, "set pm3d depthorder at bs");
	gnuplot_cmd(handle, "set view 0, 0");
	gnuplot_cmd(handle, "set cntrparam levels auto 10");
	gnuplot_cmd(handle, "set contour");
	gnuplot_cmd(handle, "set palette defined (0 \"dark-blue\", 1 \"blue\", 2 \"green\")");
//	gnuplot_cmd(handle, "splot \"plot.dat\" u 1:2:3 with lines");
	gnuplot_cmd(handle, "splot \"plot.dat\" u 1:2:3 with pm3d");

	getchar();

	gnuplot_close(handle);

	printf("Output map drawn.\nDagon bids thee farewell...\n\n");

	sqlite3_finalize(stmt);
	sqlite3_close(conn);

	nmea_parser_destroy(&nparser);
	nmea_parser_destroy(&parser_prev);


	tcsetattr(fd_RFM, TCSANOW, &oldtio); // Restore serial port settings
        tcsetattr(pstm, TCSANOW, &oldtio_pstm);
	close(fd_RFM);

	return 0;
}
